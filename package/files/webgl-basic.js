function wgl$(canvasID) {
  this.canvasID = canvasID;
  this.canvas = document.getElementById(canvasID);
  this.gl = null;
}

wgl$.prototype.initWebGL = function() {
  this.gl = this.canvas.getContext('webgl') || this.canvas.getContext('experimental-webgl');
}

wgl$.prototype.setup = function() {
  this.initWebGL();
  if(!this.gl) { this.setupSuccessful = false; return; }

  this.programs = [];
  var toCompile = this.getProgramsList();
  var errors = false;
  for(var i = 0; i < toCompile.length; i++) {
    var result = this.createProgram(toCompile[i][0], toCompile[i][1]);
    if(!result.status) {
      errors = true;
      break;
    }
    this.programs.push(result);
  }
  if(errors) {
    console.log("Was unable to create program(s).");
    for(var i = 0; i < this.programs.length; i++) {
      this.gl.deleteShader(this.programs[i].vertexShaderID);
      this.gl.deleteShader(this.programs[i].fragmentShaderID);
      this.gl.deleteProgram(this.programs[i].programID);
    }
    this.programs.length = 0;
    this.setupSuccessful = false;
    return;
  }
  this.afterCreateProgram();
  
  this.buffers = {};
  if(!this.initBuffers()) {
    this.setupSuccessful = false;
    return;
  }

  this.setupSuccessful = true;

  this.afterSetup();
}

wgl$.prototype.compileShader = function(shaderSource, shaderType) {
  if(!shaderSource) {
    return null;
  }

  var shaderGLType = null;
  if(shaderType == this.gl.FRAGMENT_SHADER || shaderType == 'x-shader/x-fragment') {
    shaderGLType = this.gl.FRAGMENT_SHADER;
  } else if(shaderType == this.gl.VERTEX_SHADER || shaderType == 'x-shader/x-vertex') {
    shaderGLType = this.gl.VERTEX_SHADER;
  } else {
    return null;
  }

  var shaderID = this.gl.createShader(shaderGLType);

  this.gl.shaderSource(shaderID, shaderSource);
  this.gl.compileShader(shaderID);  

  if (!this.gl.getShaderParameter(shaderID, this.gl.COMPILE_STATUS)) {  
      console.log('Compile error: ' + this.gl.getShaderInfoLog(shaderID));  
      this.gl.deleteShader(shaderID);
      return null;  
  }

  return shaderID;
}

wgl$.prototype.createProgram = function(vertexSource, fragmentSource) {
  var vertexShaderID = this.compileShader(vertexSource, this.gl.VERTEX_SHADER);
  var fragmentShaderID = this.compileShader(fragmentSource, this.gl.FRAGMENT_SHADER);

  if(vertexShaderID==null) return {status: false};
  if(fragmentShaderID==null) {
    this.gl.deleteShader(vertexShaderID);
    return {status: false};
  }

  var programID = this.gl.createProgram();
  this.gl.attachShader(programID, vertexShaderID);
  this.gl.attachShader(programID, fragmentShaderID);
  this.gl.linkProgram(programID);
  this.gl.detachShader(programID, fragmentShaderID);
  this.gl.detachShader(programID, vertexShaderID);
  
  if (!this.gl.getProgramParameter(programID, this.gl.LINK_STATUS)) {
    console.log('Program linking error: ' + this.gl.getProgramInfoLog(programID));
    this.gl.deleteShader(vertexShaderID);
    this.gl.deleteShader(fragmentShaderID);
    this.gl.deleteProgram(programID);
    return {status: false};
  }
  
  return {
    status: true, 
    vertexShaderID: vertexShaderID, 
    fragmentShaderID: fragmentShaderID, 
    programID: programID
  };
}

wgl$.prototype.afterCreateProgram = function() {
}

wgl$.prototype.afterSetup = function() {
}

wgl$.prototype.getProgramsList = function() {
  return [
    [
      '  attribute vec3 vertexPosition;\n' +
      '\n' +
      '  void main(void) {\n' +
      '    gl_Position = vec4(vertexPosition, 1.0);\n' +
      '  }',

      '  void main(void) {\n' +
      '    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n' +
      '  }\n'
    ]
  ];
}

wgl$.prototype.initBuffers = function() {
  return true;
}

wgl$.prototype.display = function() {
  this.gl.clearColor(0.0, 0.0, 1.0, 1.0);
  this.gl.enable(this.gl.DEPTH_TEST);
  this.gl.depthFunc(this.gl.LEQUAL);
  this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
}

wgl$.flatten = function(arr) {
  var res = [];
  arr.forEach(function f(elem) {
    if(Array.isArray(elem)) { elem.forEach(f); } else { res.push(elem); }
  });
  return res;
}

wgl$.identity = function() {
  return [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]];
}

// Creates cube (min,max)x(min,max)x(min,max).
function m$Cube(min,max) {
  var vertexes = [
    [min, min, min],
    [min, min, max],
    [max, min, min],
    [max, min, max],
    [max, max, min],
    [max, max, max],
    [min, max, min],
    [min, max, max]
  ];
  var indexes = [
    [0, 2, 1],
    [2, 3, 1],
    [2, 4, 3],
    [4, 5, 3],
    [4, 6, 5],
    [6, 7, 5],
    [6, 0, 7],
    [0, 1, 7],
    [0, 4, 2],
    [0, 6, 4],
    [1, 3, 5],
    [1, 5, 7]
  ];
  this.vertexes = vertexes;
  this.indexes = indexes;
  this.asTriangles = function() {
    var arr = [];
    for(var i = 0; i < this.indexes.length; i++) {
      arr.push(this.vertexes[this.indexes[i][0]]);
      arr.push(this.vertexes[this.indexes[i][1]]);
      arr.push(this.vertexes[this.indexes[i][2]]);
    }
    return arr;
  }
}

