  function generateSphere(radius, subdivisionsDepth) {
    let center = v$([0,0,0]), 
        p1 = [radius,0,0],
        p2 = [radius*Math.cos(2*Math.PI/3),radius*Math.sin(2*Math.PI/3),0],
        p3 = [radius*Math.cos(4*Math.PI/3),radius*Math.sin(4*Math.PI/3),0],
        pU = [0,0,radius],
        pD = [0,0,-radius];
    if(subdivisionsDepth<1) subdivisionsDepth = 1;
    let vertices = [p1,p2,p3,pU,pD];
    let triangles = [[0,1,3], [0,4,1], [2,0,3], [2,4,0], [1,2,3], [4,2,1]];
    let normals = [
      v$(vertices[0]).sub(center).normalized().elems(),
      v$(vertices[1]).sub(center).normalized().elems(),
      v$(vertices[2]).sub(center).normalized().elems(),
      v$(vertices[3]).sub(center).normalized().elems(),
      v$(vertices[4]).sub(center).normalized().elems()
    ];
    for(var depth = 2; depth <= subdivisionsDepth; depth++) {
      let ntr = [];
      for(var i = 0; i < triangles.length; i++) {
        let tri = triangles[i];
        let c1 = v$(vertices[tri[0]]).add(vertices[tri[1]]).multiplyWithScalar(0.5).normalized().multiplyWithScalar(radius).elems();
        let c2 = v$(vertices[tri[1]]).add(vertices[tri[2]]).multiplyWithScalar(0.5).normalized().multiplyWithScalar(radius).elems();
        let c3 = v$(vertices[tri[2]]).add(vertices[tri[0]]).multiplyWithScalar(0.5).normalized().multiplyWithScalar(radius).elems();
        let ci = vertices.length-1;
        vertices.push(c1);
        vertices.push(c2);
        vertices.push(c3);
        normals.push(v$(c1).sub(center).normalized().elems());
        normals.push(v$(c2).sub(center).normalized().elems());
        normals.push(v$(c3).sub(center).normalized().elems());
        ntr.push([tri[0],ci+1,ci+3]); 
        ntr.push([ci+1,tri[1],ci+2]); 
        ntr.push([ci+2,tri[2],ci+3]);
        ntr.push([ci+1,ci+2,ci+3]);
      }
      triangles = ntr;
    }
    this.triangles = triangles;
    this.vertices = vertices;
    this.normals = normals;

    this.exportVertexNormal = function() {
      let vn = [];
      for(var i = 0; i < this.vertices.length; i++) {
        vn.push(this.vertices[i][0]);
        vn.push(this.vertices[i][1]);
        vn.push(this.vertices[i][2]);
        vn.push(this.normals[i][0]);
        vn.push(this.normals[i][1]);
        vn.push(this.normals[i][2]);
      }
      let ts = [];
      for(var i = 0; i < this.triangles.length; i++) {
        ts.push(this.triangles[i][0]);
        ts.push(this.triangles[i][1]);
        ts.push(this.triangles[i][2]);
      }
      return {vertices: vn, triangles: ts, trianglesCount: this.triangles.length};
    }

    this.exportTriangleNormal = function() {
      let vn = [];
      let ts = [];
      for(var i = 0; i < this.triangles.length; i++) {
        let p1 = this.vertices[this.triangles[i][0]];
        let p2 = this.vertices[this.triangles[i][1]];
        let p3 = this.vertices[this.triangles[i][2]];
        let n = v$(p2).sub(p1).cross(v$(p3).sub(p1)).normalized().elems();

        vn.push(p1[0]);
        vn.push(p1[1]);
        vn.push(p1[2]);
        vn.push(n[0]);
        vn.push(n[1]);
        vn.push(n[2]);

        vn.push(p2[0]);
        vn.push(p2[1]);
        vn.push(p2[2]);
        vn.push(n[0]);
        vn.push(n[1]);
        vn.push(n[2]);

        vn.push(p3[0]);
        vn.push(p3[1]);
        vn.push(p3[2]);
        vn.push(n[0]);
        vn.push(n[1]);
        vn.push(n[2]);

        ts.push(3*i+0);
        ts.push(3*i+1);
        ts.push(3*i+2);
      }
      return {vertices: vn, triangles: ts, trianglesCount: this.triangles.length};
    }
  }
  
  function lightningDemo(canvasID) {
    var scene = new wgl$(canvasID);
    this.scene = scene;

    scene.startTime = null;
    scene.time = null;
    scene.angle = 0;
    scene.eye = null;
    scene.lightningAmbientOn = true;
    scene.lightningDiffuzeOn = true;
    scene.lightningSpecularOn = true;
    scene.lightningComponents = 7;
    scene.shininess = 30;

    scene.toggleAmbient = function(v) {
      this.lightningAmbientOn = v;
      this.lightningComponents = (this.lightningAmbientOn ? 1 : 0) + (this.lightningDiffuzeOn ? 2 : 0) + (this.lightningSpecularOn ? 4 : 0);
    }
    scene.toggleDiffuze = function(v) {
      this.lightningDiffuzeOn = v;
      this.lightningComponents = (this.lightningAmbientOn ? 1 : 0) + (this.lightningDiffuzeOn ? 2 : 0) + (this.lightningSpecularOn ? 4 : 0);
    }
    scene.toggleSpecular = function(v) {
      this.lightningSpecularOn = v;
      this.lightningComponents = (this.lightningAmbientOn ? 1 : 0) + (this.lightningDiffuzeOn ? 2 : 0) + (this.lightningSpecularOn ? 4 : 0);
    }

    scene.getProgramsList = function() {
      return [
        [
         "  attribute vec3 vertexPosition;\n" +
         "  attribute vec3 vertexNormal;\n" +
         "\n" +
         "  uniform mat4 mMatrix;\n" +
         "  uniform mat4 minvMatrix;\n" +
         "  uniform mat4 mvMatrix;\n" +
         "  uniform mat4 pMatrix;\n" +
         "  uniform vec3 eye;\n" +
         "  uniform int lcomp;\n" +
         "  uniform float shininess;\n" +
         "  uniform vec4 lightPosition;\n" +
         "  uniform vec3 lightColor;\n" +
         "  uniform vec3 ambientLight;\n" +
         "  uniform vec3 materialAmbCoefs;\n" +
         "  uniform vec3 materialDifCoefs;\n" +
         "  uniform vec3 materialRefCoefs;\n" +
         "  uniform vec3 attnFactors;\n" +
         "\n" +
         "  varying highp vec3 vColor;\n" +
         "  \n" +
         "  void main(void) {\n" +
         "    \n" +
         "    highp vec3 pos = (mMatrix * vec4(vertexPosition, 1.0)).xyz;\n" +
         "    highp vec3 nor = normalize((minvMatrix * vec4(vertexNormal, 1.0)).xyz);\n" +
         "    \n" +
         "    int directionalModel = abs(lightPosition.w)<0.000001 ? 1 : 0;\n" +
         "    \n" +
         "    highp vec3 directionalVector = directionalModel==1 ? normalize(lightPosition.xyz) : normalize((lightPosition.xyz / lightPosition.w)-pos);\n" +
         "    \n" +
         "    highp float directional = max(dot(nor.xyz, directionalVector), 0.0);\n" +
         "    highp vec3 l = directionalVector;\n" +
         "    \n" +
         "    highp vec3 reflected = 2.0*dot(l, nor)*nor - l;\n" + 
         "    highp vec3 ev = normalize(eye - pos);\n" +
         "    highp float refc = directional<0.0 ? 0.0 : dot(reflected, ev);\n" + 
         "    highp vec3 ref = refc>0.0 ? lightColor * materialRefCoefs * pow(refc, shininess) : vec3(0.0,0.0,0.0);\n" + 
         "    \n" +
         "    int tmp = lcomp;\n" +
         "    int bit0 = (tmp/2)*2 == tmp ? 0 : 1;\n" +
         "    tmp = tmp/2;\n" +
         "    int bit1 = (tmp/2)*2 == tmp ? 0 : 1;\n" +
         "    tmp = tmp/2;\n" +
         "    int bit2 = (tmp/2)*2 == tmp ? 0 : 1;\n" +
         "    highp vec3 tmpColor = vec3(0.0, 0.0, 0.0);\n" +
         "    if(bit0 == 1) tmpColor += ambientLight * materialAmbCoefs;\n" +
         "    if(bit1 == 1) tmpColor += lightColor * materialDifCoefs * directional;\n" +
         "    if(bit2 == 1) tmpColor += ref;\n" +
         "    \n" +
         "    if(directionalModel==0) {\n" +
         "      float d = length(lightPosition.xyz - pos);\n" +
         "      tmpColor /= (attnFactors[0] + attnFactors[1]*d + attnFactors[2]*d*d);\n" +
         "    }\n" +
         "    \n" +
         "    vColor = clamp(tmpColor, 0.0, 1.0);\n" +
         "    \n" +
         "    gl_Position = pMatrix * mvMatrix * vec4(vertexPosition, 1.0);\n" +
         "  }",

         "  varying highp vec3 vColor;\n" +
         "  \n" +
         "  \n" +
         "  void main(void) {\n" +
         "    gl_FragColor = vec4(vColor, 1.0);\n" +
         "  }"
        ]
      ];
    }

    scene.afterCreateProgram = function() {
    }

    scene.afterSetup = function() {
      this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
      this.gl.enable(this.gl.DEPTH_TEST);
      this.gl.depthFunc(this.gl.LEQUAL);
    }

    scene.initBuffers = function() {
      var sphere = new generateSphere(1, 6);
      var e = sphere.exportVertexNormal(); //exportTriangleNormal();

      this.xobjects = [];

      // Spremnik vrhova za kocku:
      // -------------------------
      var bufID = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, bufID);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(e.vertices), this.gl.STATIC_DRAW);
      this.buffers["vrhovi"] = bufID;

      // Spremnik indeksa za kocku:
      // -------------------------
      var bufID2 = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, bufID2);
      this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(e.triangles), this.gl.STATIC_DRAW);
      this.buffers["trokuti"] = bufID2;

      this.xobjects["sfera"] = {trianglesCount: e.trianglesCount};

      // Spremnik za koordinatne osi:
      // ----------------------------
      bufID = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, bufID);

      vertices = [0,0,0, 3,0,0, 0,0,0, 0,3,0, 0,0,0, 0,0,3];

      this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(vertices), this.gl.STATIC_DRAW);

      this.buffers["osi"] = bufID;

      return true;
    }

    scene.display = function() {
      this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

      this.gl.useProgram(this.programs[0].programID);
      var vertexPositionAttribute = this.gl.getAttribLocation(this.programs[0].programID, 'vertexPosition');
      this.gl.enableVertexAttribArray(vertexPositionAttribute);
      var vertexNormalAttribute = this.gl.getAttribLocation(this.programs[0].programID, 'vertexNormal');
      this.gl.enableVertexAttribArray(vertexNormalAttribute);

      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffers["vrhovi"]);
      this.gl.vertexAttribPointer(vertexPositionAttribute, 3, this.gl.FLOAT, false, 6*4, 0);
      this.gl.vertexAttribPointer(vertexNormalAttribute, 3, this.gl.FLOAT, false, 6*4, 3*4);

      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.buffers["trokuti"]);

      // this.frustum(-2,2,-2,2,3,10), this.translation(-0.5, -1.5, -5)
      var pMatrix = wgl$.perspectiveProjection(40*3.14/180,4/3,1,10);

      var mMatrix = m4$(wgl$.identity()).
                       mul(wgl$.translation(0, 0, 0)).
                       mul(wgl$.scaling(1,1,1));
      var minvMatrix = mMatrix.inv().transpose();
      var lam = wgl$.lookAt(this.eye,[0,0,0],[0,1,0]);
      var mvMatrix = m4$(lam).mul(mMatrix);

      var pUniformID = this.gl.getUniformLocation(this.programs[0].programID, "pMatrix");
      this.gl.uniformMatrix4fv(pUniformID, false, m4$.float32array(pMatrix));

      var mvUniformID = this.gl.getUniformLocation(this.programs[0].programID, "mvMatrix");
      this.gl.uniformMatrix4fv(mvUniformID, false, m4$.float32array(mvMatrix));

      var mUniformID = this.gl.getUniformLocation(this.programs[0].programID, "mMatrix");
      this.gl.uniformMatrix4fv(mUniformID, false, m4$.float32array(mMatrix));

      var minvUniformID = this.gl.getUniformLocation(this.programs[0].programID, "minvMatrix");
      this.gl.uniformMatrix4fv(minvUniformID, false, m4$.float32array(minvMatrix));

      var eyeUniformID = this.gl.getUniformLocation(this.programs[0].programID, "eye");
      this.gl.uniform3fv(eyeUniformID, new Float32Array(this.eye));

      var lcompUniformID = this.gl.getUniformLocation(this.programs[0].programID, "lcomp");
      this.gl.uniform1i(lcompUniformID, this.lightningComponents);

      var shininessUniformID = this.gl.getUniformLocation(this.programs[0].programID, "shininess");
      this.gl.uniform1f(shininessUniformID, this.shininess);

      let tc = this.xobjects["sfera"].trianglesCount;
      this.gl.drawElements(this.gl.TRIANGLES, tc*3, this.gl.UNSIGNED_SHORT, 0);

      var lightPositionUniformID = this.gl.getUniformLocation(this.programs[0].programID, "lightPosition");
      this.gl.uniform4fv(lightPositionUniformID, new Float32Array([-5,3,5,1]));
      var lightColorUniformID = this.gl.getUniformLocation(this.programs[0].programID, "lightColor");
      this.gl.uniform3fv(lightColorUniformID, new Float32Array([1,1,1]));
      var ambientLightUniformID = this.gl.getUniformLocation(this.programs[0].programID, "ambientLight");
      this.gl.uniform3fv(ambientLightUniformID, new Float32Array([0.2,0.0,0.0]));
      var materialAmbCoefsUniformID = this.gl.getUniformLocation(this.programs[0].programID, "materialAmbCoefs");
      this.gl.uniform3fv(materialAmbCoefsUniformID, new Float32Array([1.0,1.0,1.0]));
      var materialDifCoefsUniformID = this.gl.getUniformLocation(this.programs[0].programID, "materialDifCoefs");
      this.gl.uniform3fv(materialDifCoefsUniformID, new Float32Array([0.6,0.2,0.2]));
      var materialRefCoefsUniformID = this.gl.getUniformLocation(this.programs[0].programID, "materialRefCoefs");
      this.gl.uniform3fv(materialRefCoefsUniformID, new Float32Array([0.8,0.8,0.8]));
      var attnFactorsUniformID = this.gl.getUniformLocation(this.programs[0].programID, "attnFactors");
      this.gl.uniform3fv(attnFactorsUniformID, new Float32Array([1,0.01,0]));

      mMatrix = m4$(wgl$.identity()).
                       mul(wgl$.translation(2, 0, 0)).
                       mul(wgl$.scaling(0.5,0.5,1.0));
      minvMatrix = mMatrix.inv().transpose();
      lam = wgl$.lookAt(this.eye,[0,0,0],[0,1,0]);
      mvMatrix = m4$(lam).mul(mMatrix);

      this.gl.uniformMatrix4fv(mvUniformID, false, m4$.float32array(mvMatrix));
      this.gl.uniformMatrix4fv(mUniformID, false, m4$.float32array(mMatrix));
      this.gl.uniformMatrix4fv(minvUniformID, false, m4$.float32array(minvMatrix));

      this.gl.drawElements(this.gl.TRIANGLES, tc*3, this.gl.UNSIGNED_SHORT, 0);

      mMatrix = m4$(wgl$.identity()).
                       mul(wgl$.translation(0, 0, 2)).
                       mul(wgl$.scaling(1.0,0.5,0.5));
      minvMatrix = mMatrix.inv().transpose();
      lam = wgl$.lookAt(this.eye,[0,0,0],[0,1,0]);
      mvMatrix = m4$(lam).mul(mMatrix);

      this.gl.uniformMatrix4fv(mvUniformID, false, m4$.float32array(mvMatrix));
      this.gl.uniformMatrix4fv(mUniformID, false, m4$.float32array(mMatrix));
      this.gl.uniformMatrix4fv(minvUniformID, false, m4$.float32array(minvMatrix));

      this.gl.uniform3fv(materialDifCoefsUniformID, new Float32Array([0.2,0.6,0.2]));

      this.gl.drawElements(this.gl.TRIANGLES, tc*3, this.gl.UNSIGNED_SHORT, 0);

      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffers["osi"]);
      this.gl.vertexAttribPointer(vertexPositionAttribute, 3, this.gl.FLOAT, false, 0, 0);

      mMatrix = m4$(wgl$.identity());
      minvMatrix = mMatrix.inv().transpose();
      mvMatrix = m4$(lam).mul(mMatrix);

      this.gl.uniformMatrix4fv(mvUniformID, false, m4$.float32array(mvMatrix));
      this.gl.uniformMatrix4fv(mUniformID, false, m4$.float32array(mMatrix));
      this.gl.uniformMatrix4fv(minvUniformID, false, m4$.float32array(minvMatrix));

      this.gl.uniform3fv(materialDifCoefsUniformID, new Float32Array([0.6,0.2,0.2]));

      this.gl.drawArrays(this.gl.LINES, 0, 6);

    }

    scene.setup();

    function animate(timestamp) {
      if(!scene.startTime) scene.time = timestamp;
      scene.time = timestamp;
      var period = 5000;
      var elapsed = scene.time - scene.startTime;
      if(elapsed > period) {
        elapsed -= period;
        scene.startTime += period;
      }
      scene.angle = elapsed/period * 2*Math.PI;
      //scene.angle = 0.85 * 2*Math.PI;
      scene.eye = [5*Math.cos(scene.angle),0.7,-5*Math.sin(scene.angle)];
      scene.display();
      requestAnimationFrame(animate);
    }

    this.startAnimation = function() {
      if(scene.setupSuccessful) {
        requestAnimationFrame(animate);
      }
    }
  }

