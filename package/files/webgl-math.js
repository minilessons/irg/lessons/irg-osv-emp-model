// Basic support for vectors:

function v$(data) {
  if(!(this instanceof v$)) return new v$(data);
  if(data instanceof v$) {
    this.elements = [].concat(data.elements);
  } else if(Array.isArray(data)) {
    this.elements = data;
  } else {
    throw "Expected array or v$ object."
  }
}

v$.check = function(v,d) {
  if(v.length != d) throw "Expected "+d+"-element vector; got "+v.length+".";
}

v$.elems = function(v) {
  if(Array.isArray(v)) return v;
  if(v instanceof v$) return v.elements;
  throw "Expected array or v$ object.";
}

v$.add = function(v1,v2) {
  var res = [], n = v1.length;
  for(var i = 0; i<n; i++) {
    res.push(v1[i]+v2[i]);
  }
  return res;
}

v$.prototype.elems = function() {
  return this.elements;
}

v$.prototype.add = function(v) {
  return v$(v$.add(this.elements, v$.elems(v)));
}

v$.sub = function(v1,v2) {
  var res = [], n = v1.length;
  for(var i = 0; i<n; i++) {
    res.push(v1[i]-v2[i]);
  }
  return res;
}

v$.prototype.sub = function(v) {
  return v$(v$.sub(this.elements, v$.elems(v)));
}

v$.dot = function(v1,v2) {
  var res = 0, n = v1.length;
  for(var i = 0; i<n; i++) {
    res += v1[i]*v2[i];
  }
  return res;
}

v$.prototype.dot = function(v) {
  return v$.dot(this.elements, v$.elems(v));
}

v$.componentMul = function(v1,v2) {
  var res = [], n = v1.length;
  for(var i = 0; i<n; i++) {
    res.push(v1[i]*v2[i]);
  }
  return res;
}

v$.prototype.componentMul = function(v) {
  return v$(v$.componentMul(this.elements, v$.elems(v)));
}

v$.multiplyWithScalar = function(v,s) {
  var res = [], n = v.length;
  for(var i = 0; i<n; i++) {
    res.push(s*v[i]);
  }
  return res;
}

v$.prototype.multiplyWithScalar = function(s) {
  return v$(v$.multiplyWithScalar(this.elements, s));
}

v$.norm = function(v) {
  var res = 0, n = v.length;
  for(var i = 0; i<n; i++) {
    res += v[i]*v[i];
  }
  return Math.sqrt(res);
}

v$.prototype.norm = function() {
  return v$.norm(this.elements);
}

v$.normalized = function(v) {
  var res = [], n = v.length, norm = v$.norm(v);
  for(var i = 0; i<n; i++) {
    res.push(v[i]/norm);
  }
  return res;
}

v$.prototype.normalized = function() {
  return v$(v$.normalized(this.elements));
}

v$.cross = function(v1,v2) {
  if(v1.length != 3 || v2.length != 3) throw 'Vectors in cross product must have dimension equals to 3.';
  return [
    v1[1]*v2[2] - v1[2]*v2[1],
    v1[2]*v2[0] - v1[0]*v2[2],
    v1[0]*v2[1] - v1[1]*v2[0]
  ];
}

v$.prototype.cross = function(v) {
  return v$(v$.cross(this.elements, v$.elems(v)));
}

v$.triangleNormal = function(v1, v2, v3) {
  v$.check(v1, 3); v$.check(v2, 3); v$.check(v3, 3);
  return v$.cross( v$.sub(v$.elems(v2),v$.elems(v1)), v$.sub(v$.elems(v3),v$.elems(v1)) );
}

v$.prototype.toString = function() {
  return '[' + this.elements.join(',') + ']';
}

v$.to3d = function(v) {
  var el = v$.elems(v);
  if(el.length==3) return v$([].concat(el));
  if(el.length==4) return v$([el[0]/el[3],el[1]/el[3],el[2]/el[3]]);
  throw 'Expected 3 or 4-component vector.';
}

v$.prototype.to3d = function() {
  return v$.to3d(this);
}

// Basic support for matrices:

function m4$(data) {
  if(!(this instanceof m4$)) return new m4$(data);
  if(data instanceof m4$) {
    this.elements = [
      [].concat(data.elements[0]),
      [].concat(data.elements[1]),
      [].concat(data.elements[2]),
      [].concat(data.elements[3])
    ];
  } else if(Array.isArray(data)) {
    m4$.check(data,4,4);
    this.elements = data;
  } else {
    throw "Expected 4x4 array or m4$ object."
  }
}

m4$.copy = function(data) {
  var arr = null;
  if(data instanceof m4$) {
    arr = data.elements;
  } else if(Array.isArray(data)) {
    arr = data;
  } else {
    throw 'Expected 4x4 array or m4$ object.';
  }
  var res = [];
  var n = arr.length;
  for(var i = 0; i < n; i++) {
    res.push([].concat(arr[i]));
  }
  return res;
}

m4$.check = function(m,r,c) {
  if(m.length != r) throw "Expected "+r+"-row matrix; got "+m.length+".";
  for(var i = 0; i < r; i++) {
    if(m[i].length != c) throw "Expected "+r+"x"+c+" matrix; found col with "+m[i].length+" elements.";
  }
}

m4$.elems = function(data) {
  if(Array.isArray(data)) {
    m4$.check(data,4,4);
    return data;
  }
  if(data instanceof m4$) {
    return data.elements;
  }
  throw "Expected 4x4 array or m4$ object.";
}

m4$.prototype.elems = function() {
  return this.elements;
}

m4$.add = function(m1,m2) {
  m1 = m4$.elems(m1);
  m2 = m4$.elems(m2);
  m4$.check(m1, 4, 4);
  m4$.check(m2, 4, 4);
  return [
    [m1[0][0]+m2[0][0],m1[0][1]+m2[0][1],m1[0][2]+m2[0][2],m1[0][3]+m2[0][3]],
    [m1[1][0]+m2[1][0],m1[1][1]+m2[1][1],m1[1][2]+m2[1][2],m1[1][3]+m2[1][3]],
    [m1[2][0]+m2[2][0],m1[2][1]+m2[2][1],m1[2][2]+m2[2][2],m1[2][3]+m2[2][3]],
    [m1[3][0]+m2[3][0],m1[3][1]+m2[3][1],m1[3][2]+m2[3][2],m1[3][3]+m2[3][3]]
  ];
}

m4$.prototype.add = function(m) {
  return m4$(m4$.add(this.elements,m4$.elems(m)));
}

m4$.sub = function(m1,m2) {
  m1 = m4$.elems(m1);
  m2 = m4$.elems(m2);
  m4$.check(m1, 4, 4);
  m4$.check(m2, 4, 4);
  return [
    [m1[0][0]-m2[0][0],m1[0][1]-m2[0][1],m1[0][2]-m2[0][2],m1[0][3]-m2[0][3]],
    [m1[1][0]-m2[1][0],m1[1][1]-m2[1][1],m1[1][2]-m2[1][2],m1[1][3]-m2[1][3]],
    [m1[2][0]-m2[2][0],m1[2][1]-m2[2][1],m1[2][2]-m2[2][2],m1[2][3]-m2[2][3]],
    [m1[3][0]-m2[3][0],m1[3][1]-m2[3][1],m1[3][2]-m2[3][2],m1[3][3]-m2[3][3]]
  ];
}

m4$.prototype.sub = function(m) {
  return m4$(m4$.sub(this.elements,m4$.elems(m)));
}

m4$.transpose = function(m) {
  m = m4$.elems(m);
  m4$.check(m, 4, 4);
  return [
    [m[0][0], m[1][0], m[2][0], m[3][0]],
    [m[0][1], m[1][1], m[2][1], m[3][1]],
    [m[0][2], m[1][2], m[2][2], m[3][2]],
    [m[0][3], m[1][3], m[2][3], m[3][3]]
  ];
}

m4$.prototype.transpose = function() {
  return m4$(m4$.transpose(this.elements));
}

m4$.multiplyWithScalar = function(m, s) {
  m = m4$.elems(m);
  m4$.check(m, 4, 4);
  return [
    [m[0]*s, m[1]*s, m[2]*s, m[3]*s],
    [m[4]*s, m[5]*s, m[6]*s, m[7]*s],
    [m[8]*s, m[9]*s, m[10]*s, m[11]*s],
    [m[12]*s, m[13]*s, m[14]*s, m[15]*s]
  ];
}

m4$.prototype.multiplyWithScalar = function(s) {
  return m4$.multiplyWithScalar(this.elements,s);
}

m4$.mul = function(m1, m2) {
  m1 = m4$.elems(m1);
  m2 = m4$.elems(m2);
  m4$.check(m1, 4, 4);
  m4$.check(m2, 4, 4);
  var sum = 0;
  var res = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
  for(var r = 0; r < 4; r++) {
    for(var c = 0; c < 4; c++) {
      sum = 0;
      for(var k = 0; k < 4; k++) {
        sum += m1[r][k]*m2[k][c];
      }
      res[r][c] = sum;
    }
  }
  return res;
}

m4$.prototype.mul = function(m) {
  return m4$(m4$.mul(this.elements,m4$.elems(m)));
}

// Mnozi m*v; v moze biti 4-dimenziski ili 3-dimenzijski; rezultat je uvijek 4-dimenzijski
m4$.preMul = function(m, v) {
  m = m4$.elems(m);
  v = v$.elems(v);
  m4$.check(m, 4, 4);
  if(v.length == 4) {
    return [
      m[0][0]*v[0]+m[0][1]*v[1]+m[0][2]*v[2]+m[0][3]*v[3],
      m[1][0]*v[0]+m[1][1]*v[1]+m[1][2]*v[2]+m[1][3]*v[3],
      m[2][0]*v[0]+m[2][1]*v[1]+m[2][2]*v[2]+m[2][3]*v[3],
      m[3][0]*v[0]+m[3][1]*v[1]+m[3][2]*v[2]+m[3][3]*v[3]
    ];
  } else if(v.length == 3) {
    return [
      m[0][0]*v[0]+m[0][1]*v[1]+m[0][2]*v[2]+m[0][3],
      m[1][0]*v[0]+m[1][1]*v[1]+m[1][2]*v[2]+m[1][3],
      m[2][0]*v[0]+m[2][1]*v[1]+m[2][2]*v[2]+m[2][3],
      m[3][0]*v[0]+m[3][1]*v[1]+m[3][2]*v[2]+m[3][3]
    ];
  } else {
    throw "Expected 3 or 4-dim vector.";
  }
}

m4$.prototype.preMul = function(v) {
  return v$(m4$.preMul(this.elements,v$.elems(v)));
}

// Mnozi v*m; v moze biti 4-dimenziski ili 3-dimenzijski; rezultat je uvijek 4-dimenzijski
m4$.postMul = function(m, v) {
  m = m4$.elems(m);
  v = v$.elems(v);
  m4$.check(m, 4, 4);
  if(v.length == 4) {
    return [
      m[0][0]*v[0]+m[1][0]*v[1]+m[2][0]*v[2]+m[3][0]*v[3],
      m[0][1]*v[0]+m[1][1]*v[1]+m[2][1]*v[2]+m[3][1]*v[3],
      m[0][2]*v[0]+m[1][2]*v[1]+m[2][2]*v[2]+m[3][2]*v[3],
      m[0][3]*v[0]+m[1][3]*v[1]+m[2][3]*v[2]+m[3][3]*v[3]
    ];
  } else if(v.length == 3) {
    return [
      m[0][0]*v[0]+m[1][0]*v[1]+m[2][0]*v[2]+m[3][0],
      m[0][1]*v[0]+m[1][1]*v[1]+m[2][1]*v[2]+m[3][1],
      m[0][2]*v[0]+m[1][2]*v[1]+m[2][2]*v[2]+m[3][2],
      m[0][3]*v[0]+m[1][3]*v[1]+m[2][3]*v[2]+m[3][3]
    ];
  } else {
    throw "Expected 3 or 4-dim vector.";
  }
}

m4$.prototype.postMul = function(v) {
  return v$(m4$.postMul( this.elements, v$.elems(v) ));
}

// Metoda u polje sprema najprije sve elemente prvog retka, potom drugog retka, itd.
m4$.asArrayByRows = function(m) {
  m = m4$.elems(m);
  m4$.check(m, 4, 4);
  return [m[0][0], m[0][1], m[0][2], m[0][3], m[1][0], m[1][1], m[1][2], m[1][3], m[2][0], m[2][1], m[2][2], m[2][3], m[3][0], m[3][1], m[3][2], m[3][3]];
}

m4$.prototype.asArrayByRows = function() {
  return m4$.asArrayByRows(this.elements);
}

// Metoda u polje sprema najprije sve elemente prvog stupca, potom drugog stupca, itd.
// Vazno: naredbe poput uniformMatrix4fv ocekuju da je matrica u polje zapisana
// na ovaj nacin!!! Standardni nacin "serijalizacije" koji koristi C i slicni jezici 
// nije ovakav vec sprema redak po redak.
m4$.asArrayByColumns = function(m) {
  m = m4$.elems(m);
  m4$.check(m, 4, 4);
  return [m[0][0], m[1][0], m[2][0], m[3][0], m[0][1], m[1][1], m[2][1], m[3][1], m[0][2], m[1][2], m[2][2], m[3][2], m[0][3], m[1][3], m[2][3], m[3][3]];
}

m4$.prototype.asArrayByColumns = function() {
  return m4$.asArrayByColumns(this.elements);
}

m4$.prototype.toString = function() {
  return '['+v$(this.elements[0])+', '+v$(this.elements[1])+', '+v$(this.elements[2])+', '+v$(this.elements[3])+']';
}

m4$.prototype.float32array = function() {
  return new Float32Array(this.asArrayByColumns());
}

m4$.float32array = function(m) {
  return new Float32Array(m4$.asArrayByColumns(m));
}

m4$.inv = function(m) {
  function mkeye(n) {
    var res = [];
    for(var i = 0; i < n; i++) {
      var row = [];
      for(var j = 0; j < n; j++) row.push(i==j ? 1 : 0);
      res.push(row);
    }
    return res;
  }
  function findMaxi(m1,c) {
    var max = Math.abs(m1[c][c]), maxi = c;
    var n = m1.length;
    for(var k = c+1; k < n; k++) {
      var maxcand = Math.abs(m1[k][c]);
      if(maxcand>max) { maxi = k; maxcand=max; }
    }
    return maxi;
  }
  function addRow(m1,r1,r2) {
    var n = m1.length;
    for(var k = 0; k < n; k++) {
      m1[r1][k] += m1[r2][k];
    }
  }
  function addScaledRow(m1,r1,r2,s) {
    var n = m1.length;
    for(var k = 0; k < n; k++) {
      m1[r1][k] += m1[r2][k]*s;
    }
  }
  function scaleRow(m1,r1,s) {
    var n = m1.length;
    for(var k = 0; k < n; k++) {
      m1[r1][k] *= s;
    }
  }

  m = m4$.copy(m4$.elems(m));
  var d = m.length;
  var res = mkeye(d);
  for(var r = 0; r < d; r++) {
    var mx = findMaxi(m,r);
    if(mx != r) {
      addRow(m,r,mx);
      addRow(res,r,mx);
    }
    var sc = m[r][r];
    scaleRow(m,r,1.0/sc);
    scaleRow(res,r,1.0/sc);
    for(var rr = 0; rr < d; rr++) {
      if(rr==r) continue;
      sc = m[rr][r];
      addScaledRow(m,rr,r,-sc);
      addScaledRow(res,rr,r,-sc);
    }
  }

  return res;
}

m4$.prototype.inv = function(v) {
  return m4$(m4$.inv(this.elements));
}

