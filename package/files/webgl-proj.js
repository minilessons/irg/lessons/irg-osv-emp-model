// See: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glOrtho.xml
wgl$.orthographicProjection = function(left, right, bottom, top, nearVal, farVal) {
  var tx = -(right+left)/(right-left);
  var ty = -(top+bottom)/(top-bottom);
  var tz = -(farVal+nearVal)/(farVal-nearVal);
  return [[2/(right-left),0,0,tx],[0,2/(top-bottom),0,ty],[0,0,-2/(farVal-nearVal),tz],[0,0,0,1]];
}

// See: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glFrustum.xml
wgl$.frustum = function(left, right, bottom, top, nearVal, farVal) {
  var a = (right+left)/(right-left);
  var b = (top+bottom)/(top-bottom);
  var c = -(farVal+nearVal)/(farVal-nearVal);
  var d = -2*farVal*nearVal/(farVal-nearVal);
  return [[2*nearVal/(right-left),0,a,0],[0,2*nearVal/(top-bottom),b,0],[0,0,c,d],[0,0,-1,0]];
}

// See: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml
wgl$.perspectiveProjection = function(fovy, aspect, zNear, zFar) {
  var f = 1 / Math.tan(fovy/2);
  return [[f/aspect,0,0,0],[0,f,0,0],[0,0,-(zFar+zNear)/(zFar-zNear),-2*zFar*zNear/(zFar-zNear)],[0,0,-1,0]];
}

